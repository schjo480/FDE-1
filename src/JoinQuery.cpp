#include "JoinQuery.hpp"
#include <assert.h>
#include <iostream>
#include <fstream>
#include <thread>
#include <unordered_map>
#include <algorithm>
#include <iterator>
#include<unordered_set>
#include <sstream>
#include <string>
//---------------------------------------------------------------------------
using namespace std;
using std::unordered_map;

std::string linepath;
std::string orderpath;
std::string customerpath;


std::unordered_map<size_t,std::string> customer_map;
std::unordered_map<size_t,size_t> order_map;

uint64_t sum_auto = 0;
uint64_t sum_building = 0;
uint64_t sum_furniture = 0;
uint64_t sum_house = 0;
uint64_t sum_machinery = 0;

uint64_t count_auto = 0;
uint64_t count_building = 0;
uint64_t count_furniture = 0;
uint64_t count_house = 0;
uint64_t count_machinery = 0;


//---------------------------------------------------------------------------

JoinQuery::JoinQuery(std::string lineitem, std::string order,
                     std::string customer)
{
   linepath = lineitem;
   orderpath = order;
   customerpath = customer;

   if (customer_map.size()==0) {
      size_t tbl = read_tables();
   }


}
//---------------------------------------------------------------------------
size_t JoinQuery::read_tables() {
   //Go through the customers table
   std::ifstream tbl_customer(customerpath);
   size_t c_custkey = 0;
   size_t words_per_line_c = 0;

   for (std::string line; std::getline(tbl_customer, line, '|');) {
      if(tbl_customer.eof())
      {
         break;
      }
      if (words_per_line_c == 0) {
         c_custkey = std::stoi(line);
      } else if (words_per_line_c == 6) {
         customer_map[c_custkey] = line;
      } else if (words_per_line_c == 7) {
         words_per_line_c = 0;
         continue;
      }
      words_per_line_c++;

   }

   //Go through the orders table
   std::ifstream tbl_order(orderpath);
   size_t o_orderkey = 0;
   size_t words_per_line_o = 0;

   for (std::string line_l; std::getline(tbl_order, line_l);) {
      for (std::string line; std::getline(tbl_order, line, '|');) {
         if(tbl_order.eof())
         {
            break;
         }
         if (words_per_line_o == 0) {
            o_orderkey = std::stoi(line);
         } else if (words_per_line_o == 1) {
            order_map[o_orderkey] = std::stoi(line);
            words_per_line_o = 0;
            break;
         }
         words_per_line_o++;

      }
   }

   //Go through the lineitem table
   std::ifstream tbl_line(linepath);
   size_t l_orderkey = 0;
   size_t words_per_line_l = 0;

   for (std::string line_l; std::getline(tbl_line, line_l);)
      for (std::string line; std::getline(tbl_line, line, '|'); ) {
         if(tbl_line.eof())
         {
            break;
         }
         if (words_per_line_l == 0){
            l_orderkey = std::stoi(line);
         }
         else if(words_per_line_l == 4){
            if (customer_map[order_map[l_orderkey]]=="AUTOMOBILE") {
               sum_auto += std::stoi(line);
               count_auto++;
            }
            else if (customer_map[order_map[l_orderkey]]=="BUILDING") {
               sum_building += std::stoi(line);
               count_building++;
            }
            else if (customer_map[order_map[l_orderkey]]=="FURNITURE") {
               sum_furniture += std::stoi(line);
               count_furniture++;
            }
            else if (customer_map[order_map[l_orderkey]]=="HOUSEHOLD") {
               sum_house += std::stoi(line);
               count_house++;
            }
            else {
               sum_machinery += std::stoi(line);
               count_machinery++;
            }
            words_per_line_l = 0;
            break;
         }
         words_per_line_l++;

      }
   return 1;
}


//---------------------------------------------------------------------------
size_t JoinQuery::avg(std::string segmentParam)
{
   if (segmentParam=="MACHINERY")
   {
      return (sum_machinery*100)/count_machinery;
   }
   else if (segmentParam=="FURNITURE")
   {
      return ((sum_furniture)*100 - count_furniture)/count_furniture;
   }
   else if (segmentParam=="AUTOMOBILE")
   {
      return (sum_auto*100)/count_auto;
   }
   else if (segmentParam=="BUILDING")
   {
      return (sum_building*100)/count_building;
   }
   else
   {
      return (sum_house*100)/count_house;
   }

}
//---------------------------------------------------------------------------
size_t JoinQuery::lineCount(std::string rel)
{
   std::ifstream relation(rel);
   assert(relation);  // make sure the provided string references a file
   size_t n = 0;
   for (std::string line; std::getline(relation, line);) n++;
   return n;
}
//---------------------------------------------------------------------------
